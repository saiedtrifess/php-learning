<?php require VIEW_ROOT . '/templates/header.php'; ?>
<?php if (!$page): ?>
	<p>Sorry, no pages at the moment.</p>
<?php else: ?>
	<h2><?php echo e($page['title']); ?></h2>
	<h2><?php echo e($page['body']); ?></h2>
	<p class="faded">Created on <?php echo $page['created']->format('jS M Y'); ?> </p>
	<?php if ($page['updated']): ?>
		Last Updated :<?php echo $page['updated']->format('jS M Y'); ?>
	<?php  endif;?>
<?php endif; ?>

<?php require VIEW_ROOT . '/templates/footer.php'; ?>