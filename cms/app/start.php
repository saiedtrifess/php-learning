<?php

ini_set('display_errors', 'On');

define('APP_ROOT', __DIR__);
define('VIEW_ROOT', APP_ROOT.'/views');
define('BASE_URL', 'http://localhost:8888/learn-php/php-learning/cms');

$user = 'root';
$password = 'root';
$db = 'cms';
$host = 'localhost';
$port = 3306;

$link = mysqli_init();
$success = mysqli_real_connect( 
   $link, 				
   $host, 
   $user, 
   $password, 
   $db,
   $port
);

//$db = new PDO ('mysql:host=localhost;dbname=cms', 'root', 'root');

try{
    $db = new PDO( 'mysql:host=localhost;dbname=cms',
                    'root',
                    'root',
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
}
catch(PDOException $ex){
    die(json_encode(array('outcome' => false, 'message' => 'Unable to connect')));
}

require 'functions.php'; 